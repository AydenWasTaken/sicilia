<div class="inPageMenuLinks">
    <ul class="inPageMenuLinks">
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Starters.php')">Starters</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Salads.php')">Salads</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_MainDishes.php')">Main dishes</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Specialties.php')">Specialties</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Desserts.php')">Desserts</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Sauces.php')">Sauces</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Beverages.php')">Beverages</a><br></li>
    </ul>
</div>

<div class="menuDivs" id="menuSaladsDiv">
    <h1>Menu - salads</h1>
    <table class="menuTables" id="menuSaladsTable" border="white">
        <tr>
            <th>
                <img src="pictures/Grilled-chicken-and-wheat-berry-salad.jpg" alt="Grilled chicken salad picture" width="100" height="80">
                <p><i>Grilled Chicken and Wheat-Berry Salad</i></p>Fresh salad made with grilled chicken breast, winter wheat berries, bay leaf, spinach, green apple and red bell peppers</p><br>350 gr. €7.50
                <button type="button" onclick="AddItemToCart('Grilled Chicken and Wheat-Berry Salad','1','7.50')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/crab%20salad.jpg" alt="crab salad picture" width="100" height="80">
                <p><i>Chamorro crab salad</i></p>Salad made with brocolli, cucumber, mayonnaise and crab meat.<br>350 gr. €7.50
                <button type="button" onclick="AddItemToCart('Chamorro crab salad','1','7.50')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <img src="pictures/Italian%20salad.jpg" alt="Big italian salad picture" width="100" height="80">
                <p><i>Big Italian Salad</i></p>Salad made with garlic, oregano, red pepper, romaine heart, celery, union, olives, peperoncini, parmigiano-reggiano cheese served with sauce made from red wine vinegar<br><br>350 gr. €7.50
                <button type="button" onclick="AddItemToCart('Big Italian Salad','1','7.50')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/Honey%20lime%20fruit%20salad.jpg" alt="Honey lime fruit salad picture" width="100" height="80">
                <p><i>Honey lime fruit salad</i></p>Salad made with <br>a variety of fruits, with a dressing made from honey, lime juice and pineapple juice.<br>350 gr. €7.50
                <button type="button" onclick="AddItemToCart('Honey lime fruit salad','1','7.50')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
    </table>
</div>
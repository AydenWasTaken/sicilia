<div class="inPageMenuLinks">
    <ul class="inPageMenuLinks">
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Starters.php')">Starters</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Salads.php')">Salads</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_MainDishes.php')">Main dishes</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Specialties.php')">Specialties</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Desserts.php')">Desserts</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Sauces.php')">Sauces</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Beverages.php')">Beverages</a><br></li>
    </ul>
</div>
<div class="menuDivs" id="menuDishesDiv">
    <h1>Menu - main dishes</h1>
    <table class="menuTables" id="menuDishesTable" border="white">
        <tr>
            <th>
                <img src="pictures/pizza_margherita.jpg" alt="Pizza margherita picture" width="100" height="75">
                <p><i>Pizza Margherita</i></p>Standard pizza with tomato sauce and cheese.<br>350 gr. €9.99
                <button type="button" onclick="AddItemToCart('Pizza Margherita','1','9.99')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/Spaghetti_bolognese.jpg" alt="Spaghetti bolognese picture" width="100" height="82">
                <p><i>Spaghetti Bolognese</i></p>Spaghetti with bolognese sauce, parmesan cheese and basil.<br>350 gr. €12.50
                <button type="button" onclick="AddItemToCart('Spaghetti Bolognese','1','12.50')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <img src="pictures/pizza_hawaii.jpg" alt="Pizza picture" width="100" height="80">
                <p><i>Pizza Hawaii</i></p>Pizza with tomato sauce, cheese, ham and pineapple.<br>350 gr. €9.99
                <button type="button" onclick="AddItemToCart('Pizza Hawaii','1','9.99')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/Spaghetti%20alla%20carbonara.jpg" alt="Spaghetti alla carbonara picture" width="100" height="100">
                <p><i>Spaghetti alla carbonara</i></p>Spaghetti made with eggs,hard cheese, parmesan cheese and black pepper.<br>430 gr. €12.50
                <button type="button" onclick="AddItemToCart('Spaghetti alla carbonara','1','12.50')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <img src="pictures/Pizza_meatball.jpg" alt="Pizza meatball picture" width="100" height="82">
                <p><i>Meatball pizza</i></p>Pizza made with tomato<br>sauce, cheese and meatballs.<br>450 gr. €9.99
                <button type="button" onclick="AddItemToCart('Meatball pizza','1','9.99')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/Mac-and-cheese.jpg" alt="Mac and cheese picture" width="100" height="100">
                <p><i>Mac and cheese</i></p>Macaroni made with <br>cheddar sauce,<br>milk, butter and flour<br>350 gr. €7.50
                <button type="button" onclick="AddItemToCart('Mac and cheese','1','7.50')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
    </table>
</div>
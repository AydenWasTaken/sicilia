var productArray = []; //EXAMPLE OBJECT: var car = {type:"Fiat", model:"500", color:"white"};
var cartData;
function AddItemToCart(name, quantity, price) {
    //add to array
    for (var i = 0; i < productArray.length; i++){
        if(productArray[i].Name == name){
            productArray[i].Quantity++;
            alert("Added another " + name + " to the cart");
            return;
        }
    }
    productArray.push({Name: name, Quantity: quantity, Price: price});
    alert("Added item successfully!");
}

function DisplayCart() {
    cartData = '<table id="cartTable" border="white"><tr><th>Name</th><th>Quantity</th><th>Price per product</th><th>Price</th></tr>';
    var totalPrice = 0;
    for (var g = 0; g < productArray.length; g++) {

        cartData += "<tr><td>" + productArray[g].Name + "</td><td>" + productArray[g].Quantity + "</td><td>" + productArray[g].Price + "</td><td>" + productArray[g].Quantity * productArray[g].Price + "</td><td><button id='removeItemButton' onclick='DelElement(" + g + ")'>Remove item</button></td></tr>";
        totalPrice += (productArray[g].Price * productArray[g].Quantity);
    }
    totalPrice = totalPrice.toFixed(2);
    cartData += '<tr><td></td><td></td><td></td><td>' + totalPrice + '</td></tr></table>';
    cartData += '<button type="button" id="checkoutButton" onclick="Checkout()">Checkout</button>';
    document.getElementById('cartDiv').innerHTML = cartData;
}
function DelElement(a) {
    if (productArray[a].Quantity > 1){
        productArray[a].Quantity--;
    }
    else{
        productArray.splice(a, 1);
    }
    DisplayCart();
}
function Checkout() {
    alert('I am sorry, due to our lack of reality we are not allowed to take your money!');
}
function loadInfo(element, filename) {

    filename = filename || "HomePage.php";

    /*#region make active button with a different background*/

    if(element) /*checks if load info is being called from a button click*/
    {
        var aElements = document.getElementsByClassName('nav_btns');
        for(var i = 0; i < aElements.length; i++)
        {
            aElements[i].classList.remove('active');
        }
        element.classList.add('active');
    }
    /*#endregion*/

    /*#region change of title in tab according to the chosen one*/

    if (filename == "HomePage.php")
    {
        document.title = "Sicilia - Home"
    }
    else if (filename == "OurStory.php")
    {
        document.title = "Our story"
    }
    else if (filename == "ContactUs_ContactForm.php")
    {
        document.title = "Contact us - Contact form";
    }
    else if (filename == "ContactUs_Location.php")
    {
        document.title = "Contact us - Location";
    }
    else if (filename == "Events_PlanYourEvents.php")
    {
        document.title = "Events - Plan your event";
    }
    else if (filename == "Events_Planned.php")
    {
        document.title = "Events - Planned";
    }
    else if (filename == "Menu_Beverages.php")
    {
        document.title = "Menu - Beverages";
    }
    else if (filename == "Menu_Desserts.php")
    {
        document.title = "Menu - Desserts";
    }
    else if (filename == "Menu_MainDishes.php")
    {
        document.title = "Menu - Main Dishes";
    }
    else if (filename == "Menu_Salads.php")
    {
        document.title = "Menu - Salads";
    }
    else if (filename == "Menu_Sauces.php")
    {
        document.title = "Menu - Sauces";
    }
    else if (filename == "Menu_Specialties.php")
    {
        document.title = "Menu - Specialties";
    }
    else if (filename == "Menu_Starters.php")
    {
        document.title = "Menu - Starters";
    }
    else if (filename == "Cart.php")
    {
        document.title = "Cart";
    }
    else if (filename == "signup.php"){
        document.title = "Sign up";
        document.getElementById('loadSignupButton').classList.remove('active');
    }
    else
    {
        return;
    }
    /*#endregion*/

    // Create XMLHttpRequest instance, more info: https://www.w3schools.com/xml/xml_http.asp
    var xhttp = new XMLHttpRequest();

    // The "onreadystatechange" property specifies a function to be executed
    // every time the status of the XMLHttpRequest object changes
    xhttp.onreadystatechange = function(){
        // when readyState property is 4 and the status property is 200,
        // the response is ready, more info: https://www.w3schools.com/xml/ajax_xmlhttprequest_response.asp
        if(this.readyState == 4 && this.status == 200){
            // Here we find the content placeholder into which the retrieved data will be set
            document.getElementById("pageContent").innerHTML = this.responseText;
            if(filename == "Cart.php"){
                DisplayCart();
            }
        }
    };
    xhttp.open("GET", filename, true);
    xhttp.send();
    console.log(filename);
}

(function () {
    this.loadInfo();
})();
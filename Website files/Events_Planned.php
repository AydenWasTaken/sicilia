<?php
    session_start();
    ?>
<div class="inPageEventLinks">
    <a href = "#" onclick="loadInfo(this, 'Events_Planned.php')">Planned Events</a> <br>
    <a href = "#" onclick="loadInfo(this, 'Events_PlanYourEvents.php')">Plan your event</a>
</div>

<div id="plannedEventsPageContent">
    <h1>Planned events</h1><br><br>
    <!--<iframe id="eventsIframe" src="https://calendar.google.com/calendar/embed?title=Planned%20events&amp;mode=WEEK&amp;height=600&amp;wkst=2&amp;hl=en&amp;bgcolor=%23ffffff&amp;src=siciliarestaurant93%40gmail.com&amp;color=%231B887A&amp;ctz=Europe%2FAmsterdam" style"border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe><-->
    <form id="getEventsForm" method="post" action="PHP/getEventsAction.php">
        Choose until when you want to show events.<br><br><input type="date" name="dateToShow" value="2019-02-02"><br><br>
        <input type="submit" name="submit" id="submitFilterDatePlannedEvents">
    </form>
</div>
